# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/11 22:42:58 by frtalleu          #+#    #+#              #
#    Updated: 2020/08/17 14:21:47 by frtalleu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= cub3D

SRCS	= ./srcs/make_move/make_move.c\
		  ./srcs/make_move/get_pos.c\
		  ./srcs/game_engine/map_utils.c\
		  ./srcs/game_engine/manage_key.c\
		  ./srcs/game_engine/draw_map.c\
		  ./srcs/game_engine/draw_machine.c\
		  ./srcs/game_engine/text_init.c\
		  ./srcs/parseur/parser1.c\
		  ./srcs/parseur/parser2.c\
		  ./srcs/parseur/parser3.c\
		  ./srcs/parseur/parser4.c\
		  ./srcs/parseur/parser5.c\
		  ./srcs/parseur/parser6.c\
		  ./srcs/parseur/parser7.c\
		  ./srcs/parseur/ft_split2.c\
		  ./srcs/GNL/get_next_line.c\
		  ./srcs/GNL/get_next_line_utils.c\
		  ./srcs/raycaster/raycster1.c\
		  ./srcs/raycaster/raycster2.c\
		  ./srcs/raycaster/raycster.c\
		  ./srcs/screenshot/screenshot.c\
		  ./srcs/exit/exit.c\
		  ./srcs/check_err/check_map.c\
		  ./srcs/check_err/erreur.c\
		  ./srcs/check_err/text_err.c\
		  ./srcs/main/main.c\

OBJS		= ${SRCS:.c=.o}

CFLAGS		= -Wall -Wextra -Werror

COMMAND		= clang

RM			= /bin/rm -f

#LIB			= ./srcs/libft/libft.a ./mlx/libmlx.a

LIB			= ./srcs/libft/libft.a ./minilibx-linux/libmlx.a

#INCLUDE 	= -framework OpenGL -framework AppKit

INCLUDE		= -L. minilibx-linux/libmlx.a -lXext -lX11 -lm -lbsd

MLX			= ./minilibx-linux

.c.o:
	 @$(COMMAND) -w $(CFLAGS) ${INCLUDE} -c $< -o ${<:.c=.o} ${LIB}

${NAME}:
		@$(MAKE) -C ./srcs/libft
		@$(MAKE) -C ./minilibx-linux
		@${COMMAND} ${CFLAGS} ${INCLUDE} ${SRCS} -o ${NAME} ${LIB}

all: ${NAME}

clean:
				@$(MAKE) -C ./srcs/libft/. clean
				@$(MAKE) -C ./minilibx-linux/. clean
				@${RM} ${OBJS}

fclean:		clean
			@$(MAKE) -C ./srcs/libft/. clean
			@$(MAKE) -C ./minilibx-linux/. clean
			@${RM} ${NAME}
			@${RM} ./srcs/libft/libft.a
			@${RM} ./minilibx-linux/libmlx.a

re:			fclean all

.PHONY: all clean fclean re

