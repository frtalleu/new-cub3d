/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_move.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/18 17:35:45 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/18 17:35:47 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "../../cub3d.h"
#include <math.h>
#include <unistd.h>

t_parse	*get_where_lr(t_parse *res, double speed)
{
	if (res->keys.left == 1)
	{
		if (res->map[(int)(res->player.pos.y - res->player.dir.x * speed)]
				[(int)res->player.pos.x] != '1')
			res->player.pos.y = res->player.pos.y - res->player.dir.x * speed;
		if (res->map[(int)res->player.pos.y]
				[(int)(res->player.pos.x + res->player.dir.y * speed)] != '1')
			res->player.pos.x = res->player.pos.x + res->player.dir.y * speed;
	}
	if (res->keys.right == 1)
	{
		if (res->map[(int)(res->player.pos.y + res->player.dir.x * speed)]
				[(int)res->player.pos.x] != '1')
			res->player.pos.y = res->player.pos.y + res->player.dir.x * speed;
		if (res->map[(int)res->player.pos.y]
				[(int)(res->player.pos.x - res->player.dir.y * speed)] != '1')
			res->player.pos.x = res->player.pos.x - res->player.dir.y * speed;
	}
	return (res);
}

t_parse	*get_where(t_parse *res)
{
	double speed;

	speed = 0.06;
	if (res->keys.up > 0)
	{
		if (res->map[(int)(res->player.pos.y + res->player.dir.y * speed)]
				[(int)res->player.pos.x] != '1')
			res->player.pos.y = res->player.pos.y + res->player.dir.y * speed;
		if (res->map[(int)res->player.pos.y]
				[(int)(res->player.pos.x + res->player.dir.x * speed)] != '1')
			res->player.pos.x = res->player.pos.x + res->player.dir.x * speed;
	}
	if (res->keys.down > 0)
	{
		if (res->map[(int)(res->player.pos.y - res->player.dir.y * speed)]
				[(int)res->player.pos.x] != '1')
			res->player.pos.y = res->player.pos.y - res->player.dir.y * speed;
		if (res->map[(int)res->player.pos.y]
				[(int)(res->player.pos.x - res->player.dir.x * speed)] != '1')
			res->player.pos.x = res->player.pos.x - res->player.dir.x * speed;
	}
	return (get_where_lr(res, speed));
}

t_parse	*get_dir_right(t_parse *res, double speed)
{
	double dirx;
	double planex;

	planex = res->player.plane.x;
	dirx = res->player.dir.x;
	res->player.dir.x = dirx * cos(speed) - res->player.dir.y * sin(speed);
	res->player.dir.y = dirx * sin(speed) + res->player.dir.y * cos(speed);
	res->player.plane.x = planex * cos(speed) -
		res->player.plane.y * sin(speed);
	res->player.plane.y = planex * sin(speed) +
		res->player.plane.y * cos(speed);
	return (res);
}

t_parse	*get_dir_left(t_parse *res, double speed)
{
	double dirx;
	double planex;

	planex = res->player.plane.x;
	dirx = res->player.dir.x;
	res->player.dir.x = dirx * cos(-speed) - res->player.dir.y * sin(-speed);
	res->player.dir.y = dirx * sin(-speed) + res->player.dir.y * cos(-speed);
	res->player.plane.x = planex * cos(-speed) -
		res->player.plane.y * sin(-speed);
	res->player.plane.y = planex * sin(-speed) +
		res->player.plane.y * cos(-speed);
	return (res);
}

t_parse	*get_dir(t_parse *res)
{
	double speed;

	speed = 0.05;
	if (res->keys.tl == 1 && res->keys.tr == 0)
		res = get_dir_left(res, speed);
	if (res->keys.tr == 1 && res->keys.tl == 0)
		res = get_dir_right(res, speed);
	return (res);
}
