/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_pos.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/02 22:07:26 by frtalleu          #+#    #+#             */
/*   Updated: 2020/04/20 19:03:02 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "../../cub3d.h"
#include <math.h>
#include <unistd.h>

t_parse	*make_tp(t_parse *res)
{
	int i;
	int j;

	i = 0;
	j = 0;
	if (res->player.tp == 1)
		return (res);
	while (res->map[i] != 0)
	{
		while (res->map[i][j] != '\0')
		{
			if (((int)res->player.pos.x != j || (int)res->player.pos.y != i) &&
					res->map[i][j] == 'T' && res->player.tp == 0)
			{
				res->player.pos.x = j + 0.5;
				res->player.pos.y = i + 0.5;
				res->player.tp = 1;
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (res);
}

t_parse	*get_pos(t_parse *res)
{
	res = get_where(res);
	res = get_dir(res);
	if (res->map[(int)res->player.pos.y][(int)res->player.pos.x] == 'T')
		res = make_tp(res);
	if (res->map[(int)res->player.pos.y][(int)res->player.pos.x] != 'T')
		res->player.tp = 0;
	return (res);
}
