/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screenshot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 01:28:16 by frtalleu          #+#    #+#             */
/*   Updated: 2020/04/20 19:08:48 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "../../cub3d.h"
#include <stdlib.h>
#include <stdio.h>

void	creat_header_bmp(t_parse res, int fd)
{
	int size;
	int print;

	size = res.r.x * res.r.y * 4 + 54;
	write(fd, "BM", 2);
	write(fd, &size, 4);
	write(fd, "\0\0\0\0", 4);
	print = 54;
	write(fd, &print, 4);
	print = 40;
	write(fd, &print, 4);
	write(fd, &res.r.x, 4);
	write(fd, &res.r.y, 4);
	print = 1;
	write(fd, &print, 2);
	print = 32;
	write(fd, &print, 2);
	print = 0;
	write(fd, &print, 4);
	size = size - 54;
	write(fd, &size, 4);
	print = 2835;
	write(fd, &print, 4);
	write(fd, &print, 4);
	write(fd, "\0\0\0\0\0\0\0\0", 8);
}

void	screenshot(t_parse res)
{
	int fd;
	int i;
	int j;

	i = res.r.y - 1;
	fd = open("screenshot.bmp", O_RDWR | O_CREAT, 77777);
	creat_header_bmp(res, fd);
	while (i >= 0)
	{
		j = 0;
		while (j < res.r.x)
		{
			write(fd, &res.img.data[i * res.r.x + j], 4);
			j++;
		}
		i--;
	}
	close(fd);
	ft_exit(&res, "screenshot done");
}
