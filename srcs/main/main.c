/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 00:08:13 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 15:49:48 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include "stdio.h"
#include "../libft/libft.h"
#include "unistd.h"
#include "stdlib.h"
#include "math.h"
#include <fcntl.h>

t_parse	go_to_parser(char *str)
{
	int fd;

	fd = open(str, O_RDONLY);
	if (fd == -1)
	{
		ft_putendl_fd("erreur : PATH invlalide", 1);
		exit(0);
	}
	close(fd);
	return (parser(str));
}

int		main(int ac, char **av)
{
	t_parse res;

	if ((ac != 2 && ac != 3) || av[1][ft_strlen(av[1]) - 1] != 'b'
	|| av[1][ft_strlen(av[1]) - 2] != 'u' || av[1][ft_strlen(av[1]) - 3] != 'c'
	|| av[1][ft_strlen(av[1]) - 4] != '.')
	{
		ft_putendl_fd("erreur", 1);
		exit(0);
	}
	res = go_to_parser(av[1]);
	res.f.c = rgb_int(res.f);
	res.c.c = rgb_int(res.c);
	if (ac == 3 && ft_strncmp(av[2], "--save", 6) == 0)
	{
		res.mlx_ptr = mlx_init();
		res.img.img_ptr = mlx_new_image(res.mlx_ptr, res.r.x, res.r.y);
		res.img.data = (int *)mlx_get_data_addr(res.img.img_ptr, &res.img.bpp,
					&res.img.size_l, &res.img.endian);
		res = init_texture(res);
		make_img(&res);
		screenshot(res);
	}
	else if (ac == 2)
		draw_machine(res);
}
