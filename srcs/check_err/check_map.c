/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <frtalleu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 14:32:46 by frtalleu          #+#    #+#             */
/*   Updated: 2020/06/30 16:49:42 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../cub3d.h"
#include "../libft/libft.h"
#include <stdio.h>

void	call_exit(char **map, t_parse res)
{
	int i;

	i = 0;
	while (map[i] != NULL)
	{
		free(map[i]);
		i++;
	}
	free(map);
	ft_exit(&res, "Map ouverte");
}

void	map_is_close(char **map, int x, int y, t_parse res)
{
	map[y][x] = '1';
	if ((map[y][x + 1] == '\0' || map[y][x + 1] == ' '))
		call_exit(map, res);
	else if (map[y][x + 1] != '1')
		map_is_close(map, x + 1, y, res);
	if ((x - 1) < 0 || map[y][x - 1] == ' ')
		call_exit(map, res);
	else if (map[y][x - 1] != '1')
		map_is_close(map, x - 1, y, res);
	if (map[y + 1] == NULL || (int)ft_strlen(map[y + 1]) <= x ||
		map[y + 1][x] == ' ')
		call_exit(map, res);
	else if (map[y + 1][x] != '1')
		map_is_close(map, x, y + 1, res);
	if (y - 1 < 0 || (int)ft_strlen(map[y - 1]) <= x || map[y - 1][x] == ' ')
		call_exit(map, res);
	else if (map[y - 1][x] != '1')
		map_is_close(map, x, y - 1, res);
}

void	cp_line(char *line, char *cp_line)
{
	int i;

	i = 0;
	while (line[i] != '\0')
	{
		cp_line[i] = line[i];
		i++;
	}
	cp_line[i] = '\0';
}

char	**copy_map(char **map)
{
	char	**cp_map;
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (map[i])
		i++;
	if (!(cp_map = malloc(sizeof(char *) * (i + 1))))
		return (NULL);
	cp_map[i] = NULL;
	i = 0;
	while (map[i])
	{
		while (map[i][j] != '\0')
			j++;
		if (!(cp_map[i] = malloc(sizeof(char) * (j + 1))))
			return (NULL);
		cp_line(map[i], cp_map[i]);
		j = 0;
		i++;
	}
	return (cp_map);
}

void	map_maybe_good(t_parse res)
{
	char	**cp_map;
	int		i;

	i = 0;
	cp_map = copy_map(res.map);
	map_is_close(cp_map, res.player.init_pos.x, res.player.init_pos.y, res);
	while (cp_map[i])
	{
		free(cp_map[i]);
		i++;
	}
	free(cp_map);
}
