/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   erreur.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 03:00:24 by frtalleu          #+#    #+#             */
/*   Updated: 2020/04/20 19:01:01 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

void	nb_elem(t_parse res)
{
	if (res.check.r == 1 && res.check.no == 1 && res.check.so == 1
		&& res.check.we == 1 && res.check.ea == 1 && res.check.s == 1
		&& res.check.f == 1 && res.check.c == 1 && res.check.pos == 1)
		return ;
	ft_exit_bis(&res, "Error doublon ou manquant");
}

t_parse	get_some_error(t_parse res)
{
	if (res.f.r < 0 || res.f.r > 255 || res.f.g < 0 || res.f.g > 255 ||
		res.f.b < 0 || res.f.g > 255 || res.c.r < 0 || res.c.r > 255 ||
		res.c.g < 0 || res.c.g > 255 || res.f.r < 0 || res.f.r > 255)
		ft_exit_bis(&res, "Error couleur");
	if (res.r.x > X_SCREEN)
		res.r.x = X_SCREEN;
	if (res.r.y > Y_SCREEN)
		res.r.y = Y_SCREEN;
	return (res);
}

t_parse	erreur(t_parse res)
{
	nb_elem(res);
	res = get_some_error(res);
	return (res);
}
