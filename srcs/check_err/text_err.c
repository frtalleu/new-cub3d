/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   text_err.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/23 12:43:41 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/23 12:43:43 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include <unistd.h>
#include <stdlib.h>

void	check_text(t_parse res)
{
	if (res.wno.img_ptr == NULL || res.wso.img_ptr == NULL ||
		res.wea.img_ptr == NULL || res.text_sp.img_ptr == NULL ||
		res.wwe.img_ptr == NULL || res.port.img_ptr == NULL)
		ft_exit_bis(&res, "erreur texture");
}
