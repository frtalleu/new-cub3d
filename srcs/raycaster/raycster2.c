/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycster2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/18 15:58:17 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/18 15:58:19 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "../libft/libft.h"

t_parse	*sprite_dist(t_parse *res)
{
	int	i;

	i = 0;
	while (i < res->nb_sprite)
	{
		res->sprite[i].dist = ((res->player.pos.x - res->sprite[i].x) *
			(res->player.pos.x - res->sprite[i].x)) + ((res->player.pos.y
			- res->sprite[i].y) * (res->player.pos.y - res->sprite[i].y));
		i++;
	}
	return (res);
}

t_parse	*sort_sprite(t_parse *res)
{
	t_sprite	tmp;
	int			i;

	i = 1;
	while (i < res->nb_sprite)
	{
		if (res->sprite[i].dist > res->sprite[i - 1].dist)
		{
			tmp = res->sprite[i];
			res->sprite[i] = res->sprite[i - 1];
			res->sprite[i - 1] = tmp;
			i = 1;
		}
		i++;
	}
	return (res);
}

t_parse	*sprite_to_img(t_parse *res, t_point size, int screen, t_img sprite)
{
	int		col;
	t_point	tex;
	int		j;

	while (res->fsprite.sizex.x < res->fsprite.sizex.y)
	{
		tex.x = (int)(256 * (res->fsprite.sizex.x - (-size.x / 2 + screen)) *
				sprite.width / size.x) / 256;
		j = res->fsprite.sizey.x;
		if (res->fsprite.transf.y > 0 && res->fsprite.sizex.x > 0 &&
				res->fsprite.sizex.x < res->r.x &&
				res->fsprite.transf.y < res->buffer[res->fsprite.sizex.x])
			while (j < res->fsprite.sizey.y)
			{
				tex.y = (int)((j * 256 - res->r.y * 128 + size.y * 128) *
						sprite.height) / size.y / 256;
				if (tex.y * sprite.width + tex.x > 0)
					col = sprite.data[tex.y * sprite.width + tex.x];
				if (col != 0)
					res->img.data[j * res->r.x + res->fsprite.sizex.x] = col;
				j++;
			}
		res->fsprite.sizex.x++;
	}
	return (res);
}

t_parse	*get_size_y(t_parse *res, t_point size, int screen)
{
	size.y = (int)(fabs(res->r.y / res->fsprite.transf.y));
	res->fsprite.sizey.x = (int)(-size.y / 2 + res->r.y / 2);
	res->fsprite.sizey.y = (int)(size.y / 2 + res->r.y / 2);
	if (res->fsprite.sizey.x < 0)
		res->fsprite.sizey.x = 0;
	if (res->fsprite.sizey.y >= res->r.y)
		res->fsprite.sizey.y = res->r.y - 1;
	if (res->sprite[res->x].type == 'T')
		return (sprite_to_img(res, size, screen, res->port));
	return (sprite_to_img(res, size, screen, res->text_sp));
}

t_parse	*get_size_x(t_parse *res)
{
	int		screen;
	t_point	size;

	screen = (int)((res->r.x / 2) * (1 +
				res->fsprite.transf.x / res->fsprite.transf.y));
	size.x = (int)(fabs(res->r.y / res->fsprite.transf.y));
	res->fsprite.sizex.x = -size.x / 2 + screen;
	if (res->fsprite.sizex.x < 0)
		res->fsprite.sizex.x = 0;
	res->fsprite.sizex.y = size.x / 2 + screen;
	if (res->fsprite.sizex.y >= res->r.x)
		res->fsprite.sizex.y = res->r.x - 1;
	return (get_size_y(res, size, screen));
}
