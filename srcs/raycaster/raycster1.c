/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycster1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/18 15:48:22 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/18 15:48:23 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "../libft/libft.h"

t_parse	*get_dist(t_parse *res)
{
	if (res->ray.x < 0)
	{
		res->step.x = -1;
		res->side.x = ((res->player.pos.x - res->wall_pos.x) * res->delta.x);
	}
	else
	{
		res->step.x = 1;
		res->side.x = ((res->wall_pos.x + 1.0 - res->player.pos.x)
				* res->delta.x);
	}
	if (res->ray.y < 0)
	{
		res->step.y = -1;
		res->side.y = ((res->player.pos.y - res->wall_pos.y) * res->delta.y);
	}
	else
	{
		res->step.y = 1;
		res->side.y = ((res->wall_pos.y + 1.0 - res->player.pos.y)
				* res->delta.y);
	}
	return (res);
}

t_parse	*get_dist_wall(t_parse *res)
{
	while (res->wall == 0)
	{
		if (res->side.x < res->side.y)
		{
			res->side.x = res->side.x + res->delta.x;
			res->wall_pos.x = res->wall_pos.x + res->step.x;
			res->nsew = 0;
		}
		else
		{
			res->side.y = res->side.y + res->delta.y;
			res->wall_pos.y = res->wall_pos.y + res->step.y;
			res->nsew = 1;
		}
		if (res->map[res->wall_pos.y][res->wall_pos.x] == '1')
			res->wall = 1;
	}
	return (res);
}

t_parse	*put_text(t_img tex, t_parse *res, int start, int end)
{
	int	texx;
	int	texy;
	int	i;

	i = -1;
	texx = (int)(res->wallx * (double)tex.width);
	if ((res->nsew == 0 && res->ray.x < 0)
			|| (res->nsew == 1 && res->ray.y > 0))
		texx = tex.width - texx - 1;
	while (++i < (start))
		res->img.data[res->r.x * i + res->x] = res->c.c;
	while (i < end)
	{
		texy = ((double)i * 2 - res->r.y + res->wall_pixel)
			* (double)(tex.height / 2) / res->wall_pixel;
		res->img.data[res->r.x * i + res->x] =
			tex.data[tex.height * texy + texx];
		i++;
	}
	i--;
	while (i++ < res->r.y)
		res->img.data[res->r.x * i + res->x] = res->f.c;
	return (res);
}

t_parse	*draw_column(t_parse *res)
{
	int start;
	int end;

	start = (int)(res->r.y / 2) - (res->wall_pixel / 2);
	end = (int)(res->r.y / 2) + (res->wall_pixel / 2);
	if (start < 0)
		start = 0;
	if (end >= res->r.y)
		end = res->r.y;
	if (res->nsew == 1 && res->ray.y < 0)
		res = put_text(res->wso, res, start, end);
	else if (res->nsew == 1)
		res = put_text(res->wno, res, start, end);
	else if (res->nsew == 0 && res->ray.x < 0)
		res = put_text(res->wea, res, start, end);
	else if (res->nsew == 0)
		res = put_text(res->wwe, res, start, end);
	return (res);
}

double	draw_wall(t_parse *res)
{
	double	wall_dist;

	if (res->nsew == 0)
	{
		wall_dist = ((double)((double)res->wall_pos.x - res->player.pos.x
					+ (1 - (double)res->step.x) / 2) / (double)res->ray.x);
		res->wallx = res->player.pos.y + wall_dist * res->ray.y;
	}
	else
	{
		wall_dist = ((double)((double)res->wall_pos.y - res->player.pos.y
					+ (1 - (double)res->step.y) / 2) / (double)res->ray.y);
		res->wallx = res->player.pos.x + wall_dist * res->ray.x;
	}
	res->wallx = res->wallx - floor(res->wallx);
	res->wall_pixel = (int)(res->r.y / wall_dist);
	res = draw_column(res);
	return (wall_dist);
}
