/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycster.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 01:16:14 by frtalleu          #+#    #+#             */
/*   Updated: 2020/04/20 19:06:05 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "../libft/libft.h"

void	aff_sprite(t_parse *res)
{
	res->x = 0;
	while (res->x < res->nb_sprite)
	{
		res->fsprite.sp.x = res->sprite[res->x].x - res->player.pos.x;
		res->fsprite.sp.y = res->sprite[res->x].y - res->player.pos.y;
		res->fsprite.corr = 1.0 / (double)(res->player.plane.x *
				res->player.dir.y - res->player.plane.y * res->player.dir.x);
		res->fsprite.transf.x = res->fsprite.corr * (res->player.dir.y
				* res->fsprite.sp.x - res->player.dir.x * res->fsprite.sp.y);
		res->fsprite.transf.y = res->fsprite.corr * (-res->player.plane.y *
				res->fsprite.sp.x + res->player.plane.x * res->fsprite.sp.y);
		res = get_size_x(res);
		res->x++;
	}
}

void	make_sprite(t_parse *res)
{
	res = sprite_dist(res);
	res = sort_sprite(res);
	aff_sprite(res);
}

void	raycaster(t_parse *res)
{
	res->x = 0;
	while (res->x < res->r.x)
	{
		res->cam = (double)((double)(res->x * 2) / (double)res->r.x) - 1.0;
		res->ray.x = (res->player.dir.x) + (res->player.plane.x) * res->cam;
		res->ray.y = (res->player.dir.y) + (res->player.plane.y) * res->cam;
		res->delta.x = (double)sqrt(1.0 + ((double)(res->ray.y * res->ray.y)
					/ (double)(res->ray.x * res->ray.x)));
		res->delta.y = (double)sqrt(1.0 + ((double)(res->ray.x * res->ray.x)
					/ (double)(res->ray.y * res->ray.y)));
		res->wall = 0;
		res->wall_pos.x = (int)res->player.pos.x;
		res->wall_pos.y = (int)res->player.pos.y;
		res = get_dist(res);
		res = get_dist_wall(res);
		res->buffer[res->x] = draw_wall(res);
		res->x++;
	}
	make_sprite(res);
}
