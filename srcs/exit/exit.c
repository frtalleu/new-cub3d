/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/12 04:05:07 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 12:10:57 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include "../../minilibx-linux/mlx.h"
#include "../../cub3d.h"
#include <stdlib.h>
#include <stdio.h>

void	ft_exit_bis(t_parse *res, char *msg)
{
	int	i;

	i = -1;
	ft_putendl_fd(msg, 1);
	if (res->no != 0)
		free(res->no);
	if (res->we != 0)
		free(res->we);
	if (res->ea != 0)
		free(res->ea);
	if (res->so != 0)
		free(res->so);
	if (res->s != 0)
		free(res->s);
	if (res->map != NULL)
	{
		while (res->map[++i] != 0)
			free(res->map[i]);
		free(res->map[i]);
		free(res->map);
	}
	exit(0);
}

void	ft_exit(t_parse *res, char *msg)
{
	int i;

	i = -1;
	ft_putendl_fd(msg, 0);
	free(res->no);
	free(res->we);
	free(res->ea);
	free(res->so);
	free(res->s);
	while (res->map[++i] != 0)
		free(res->map[i]);
	free(res->map[i]);
	free(res->map);
	mlx_destroy_image(res->mlx_ptr, res->wso.img_ptr);
	mlx_destroy_image(res->mlx_ptr, res->wno.img_ptr);
	mlx_destroy_image(res->mlx_ptr, res->wwe.img_ptr);
	mlx_destroy_image(res->mlx_ptr, res->wea.img_ptr);
	mlx_destroy_image(res->mlx_ptr, res->text_sp.img_ptr);
	mlx_destroy_image(res->mlx_ptr, res->port.img_ptr);
	mlx_destroy_image(res->mlx_ptr, res->img.img_ptr);
	if (res->win_ptr != NULL)
		mlx_destroy_window(res->mlx_ptr, res->win_ptr);
	exit(0);
}

int		go_to_exit(t_parse *res)
{
	ft_exit(res, "GAME OVER");
	exit(0);
}
