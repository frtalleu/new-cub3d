/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   text_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/23 15:16:01 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/23 15:16:03 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"

t_parse	init_txt_img(t_parse res)
{
	res.wno.data = (int *)mlx_get_data_addr(res.wno.img_ptr, &res.wno.bpp,
			&res.wno.size_l, &res.wno.endian);
	res.wso.data = (int *)mlx_get_data_addr(res.wso.img_ptr, &res.wso.bpp,
			&res.wso.size_l, &res.wso.endian);
	res.wea.data = (int *)mlx_get_data_addr(res.wea.img_ptr, &res.wea.bpp,
			&res.wea.size_l, &res.wea.endian);
	res.wwe.data = (int *)mlx_get_data_addr(res.wwe.img_ptr, &res.wwe.bpp,
			&res.wwe.size_l, &res.wwe.endian);
	res.text_sp.data = (int *)mlx_get_data_addr(res.text_sp.img_ptr,
			&res.text_sp.bpp, &res.text_sp.size_l, &res.text_sp.endian);
	res.port.data = (int *)mlx_get_data_addr(res.port.img_ptr, &res.port.bpp,
			&res.port.size_l, &res.port.endian);
	return (res);
}

t_parse	init_texture(t_parse res)
{
	res.wno.img_ptr = mlx_xpm_file_to_image(res.mlx_ptr, res.no, &res.wno.width,
			&res.wno.height);
	res.wso.img_ptr = mlx_xpm_file_to_image(res.mlx_ptr, res.so, &res.wso.width,
			&res.wso.height);
	res.wea.img_ptr = mlx_xpm_file_to_image(res.mlx_ptr, res.ea, &res.wea.width,
			&res.wea.height);
	res.text_sp.img_ptr = mlx_xpm_file_to_image(res.mlx_ptr, res.s,
			&res.text_sp.width, &res.text_sp.height);
	res.wwe.img_ptr = mlx_xpm_file_to_image(res.mlx_ptr, res.we, &res.wwe.width,
			&res.wwe.height);
	res.port.img_ptr = mlx_xpm_file_to_image(res.mlx_ptr,
			"./text/t_sparkle.xpm", &res.port.width, &res.port.height);
	check_text(res);
	return (init_txt_img(res));
}
