/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_machine.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 15:40:49 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 15:40:55 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include "stdio.h"
#include "../libft/libft.h"
#include "unistd.h"
#include "stdlib.h"
#include "math.h"

void	make_img(t_parse *res)
{
	double buff[res->r.x];

	res->buffer = buff;
	raycaster(res);
}

int		draw(t_parse *res)
{
	make_img(res);
	if (res->keys.map == 1 && res->r.x >= 500 && res->r.y >= 500)
		draw_map(res);
	res = get_pos(res);
	mlx_put_image_to_window(res->mlx_ptr, res->win_ptr, res->img.img_ptr, 0, 0);
	return (0);
}

void	do_loop(t_parse res)
{
	mlx_do_key_autorepeatoff(res.mlx_ptr);
	mlx_hook(res.win_ptr, 2, 1L << 0, keypress, &res);
	mlx_hook(res.win_ptr, 3, 1L << 1, keyrelease, &res);
	mlx_hook(res.win_ptr, 17, 1L << 17, go_to_exit, &res);
	mlx_loop_hook(res.mlx_ptr, draw, &res);
	mlx_loop(res.mlx_ptr);
}

void	draw_machine(t_parse res)
{
	res.mlx_ptr = mlx_init();
	res.win_ptr = mlx_new_window(res.mlx_ptr, res.r.x, res.r.y, "cub3D");
	res.img.img_ptr = mlx_new_image(res.mlx_ptr, res.r.x, res.r.y);
	res.img.data = (int *)mlx_get_data_addr(res.img.img_ptr, &res.img.bpp,
			&res.img.size_l, &res.img.endian);
	res = init_texture(res);
	map_maybe_good(res);
	do_loop(res);
}
