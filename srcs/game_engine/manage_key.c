/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_key.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 14:44:15 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 14:44:16 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include "stdio.h"
#include "../libft/libft.h"
#include "unistd.h"
#include "stdlib.h"
#include "math.h"

int	keyrelease(int key, t_parse *res)
{
	if (key == KEY_UP || key == KEY_UP_BIS)
		res->keys.up--;
	if (key == KEY_DOWN || key == KEY_DOWN_BIS)
		res->keys.down--;
	if (key == KEY_LEFT)
		res->keys.left = 0;
	if (key == KEY_RIGHT)
		res->keys.right = 0;
	if (key == KEY_TURN_L)
		res->keys.tl = 0;
	if (key == KEY_TURN_R)
		res->keys.tr = 0;
	if (res->keys.up < 0)
		res->keys.up = 0;
	if (res->keys.down < 0)
		res->keys.down = 0;
	return (key);
}

int	keypress(int key, t_parse *res)
{
	if (key == KEY_ESC)
		ft_exit(res, "GAME OVER");
	if (key == KEY_MAP)
		res->keys.map = (res->keys.map > 0) ? 0 : 1;
	if (key == KEY_UP || key == KEY_UP_BIS)
		res->keys.up++;
	if (key == KEY_DOWN || key == KEY_DOWN_BIS)
		res->keys.down++;
	if (key == KEY_LEFT)
		res->keys.left = 1;
	if (key == KEY_RIGHT)
		res->keys.right = 1;
	if (key == KEY_TURN_L)
		res->keys.tl = 1;
	if (key == KEY_TURN_R)
		res->keys.tr = 1;
	return (key);
}
