/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 14:49:26 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 14:49:28 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include "stdio.h"
#include "../libft/libft.h"
#include "unistd.h"
#include "stdlib.h"
#include "math.h"

t_parse	*set_map(t_parse *res, t_point cub, int a)
{
	int		color;
	t_point	pos;
	t_point	pixel;

	pos.x = -1;
	pos.y = 0;
	while (res->map[pos.y] != 0)
	{
		while (res->map[pos.y][++pos.x] != '\0')
		{
			color = 0xffffff;
			pixel.x = a + (cub.x * pos.x);
			pixel.y = a + (cub.y * pos.y);
			if (res->map[pos.y][pos.x] == '1')
				color = 0xC1C8C7;
			if (pos.y == (int)(res->player.pos.y)
					&& pos.x == (int)(res->player.pos.x))
				color = 0x000000;
			if (res->map[pos.y][pos.x] != ' ')
				res = make_rectangle(res, color, cub, pixel);
		}
		pos.x = -1;
		pos.y++;
	}
	return (res);
}

t_parse	*draw_map(t_parse *res)
{
	float	a;
	t_point	size_cub;
	float	cub_x;
	float	cub_y;

	cub_x = (float)(((res->r.x + res->r.y) / 2) * (25.0 / 100.0));
	cub_y = (float)(((res->r.y + res->r.x) / 2) * (25.0 / 100.0));
	a = (float)((1.0 / 100.0) * (res->r.x + res->r.y) / 2.0);
	cub_x = (float)(cub_x / x_map(res->map));
	cub_y = (float)(cub_y / y_map(res->map));
	size_cub.x = (int)((cub_x + cub_y) / 2);
	size_cub.y = (int)((cub_y + cub_x) / 2);
	res = set_map(res, size_cub, (int)a);
	return (res);
}
