/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/19 12:32:38 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 12:32:40 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../cub3d.h"
#include "../../minilibx-linux/mlx.h"
#include "stdio.h"
#include "../libft/libft.h"
#include "unistd.h"
#include "stdlib.h"
#include "math.h"

int			x_map(char **str)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[i] != 0)
	{
		if ((int)ft_strlen(str[i]) > j)
			j = ft_strlen(str[i]);
		i++;
	}
	return (j);
}

int			rgb_int(t_color color)
{
	int	rgb;

	rgb = color.r;
	rgb = (rgb << 8) + color.g;
	rgb = (rgb << 8) + color.b;
	return (rgb);
}

int			y_map(char **str)
{
	int i;

	i = 0;
	while (str[i] != 0)
		i++;
	return (i);
}

t_parse		*make_rectangle(t_parse *res, int color, t_point cub, t_point pixel)
{
	t_point print;

	print.x = pixel.x;
	print.y = pixel.y;
	while (print.x < pixel.x + cub.x)
	{
		while (print.y < pixel.y + cub.y)
		{
			res->img.data[print.y * res->r.x + print.x] = color;
			print.y++;
		}
		print.y = pixel.y;
		print.x++;
	}
	return (res);
}
