/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 12:53:53 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/17 12:58:17 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

t_key		init_key(void)
{
	t_key	key;

	key.up = 0;
	key.down = 0;
	key.left = 0;
	key.right = 0;
	key.tl = 0;
	key.map = 0;
	key.tr = 0;
	return (key);
}

t_parse		init_res(void)
{
	t_parse res;

	res.no = NULL;
	res.so = NULL;
	res.we = NULL;
	res.ea = NULL;
	res.s = NULL;
	res.f = color_init();
	res.c = color_init();
	res.check = check_init();
	res.r = point_init();
	res.mlx_ptr = NULL;
	res.win_ptr = NULL;
	res.map = NULL;
	res.img = img_init();
	res.keys = init_key();
	res.nb_sprite = 0;
	return (res);
}

t_color		pars_color(char *str, t_color color, t_parse res)
{
	int i;

	i = 1;
	color.r = ft_atoi(&str[i]);
	while (str[i] != ',' && str[i] != '\0')
		i++;
	if (str[i] == '\0')
		ft_exit_bis(&res, "Error mauvais format couleur");
	i++;
	color.g = ft_atoi(&str[i]);
	while (str[i] != ',' && str[i] != '\0')
		i++;
	if (str[i] == '\0')
		ft_exit_bis(&res, "Error mauvais format couleur");
	i++;
	color.b = ft_atoi(&str[i]);
	return (color);
}

t_parse		parstr(t_parse res, char *str)
{
	check_line_config(str, res);
	if (str[0] == 'R' && str[1] == ' ' && (res.check.r = res.check.r + 1) < 2)
		res = parse_r(str, res);
	if (str[0] == 'N' && str[1] == 'O' && str[2] == ' ' &&
			(res.check.no = res.check.no + 1) < 2)
		res.no = pars_path(str, res.no, res);
	if (str[0] == 'S' && str[1] == 'O' && str[2] == ' ' &&
			(res.check.so = res.check.so + 1))
		res.so = pars_path(str, res.so, res);
	if (str[0] == 'W' && str[1] == 'E' && str[2] == ' ' &&
			(res.check.we = res.check.we + 1) < 2)
		res.we = pars_path(str, res.we, res);
	if (str[0] == 'E' && str[1] == 'A' && str[2] == ' ' &&
			(res.check.ea = res.check.ea + 1) < 2)
		res.ea = pars_path(str, res.ea, res);
	if (str[0] == 'S' && str[1] == ' ' && (res.check.s = res.check.s + 1) < 2)
		res.s = pars_path(str, res.s, res);
	if (str[0] == 'F' && str[1] == ' ' && (res.check.f = res.check.f + 1) < 2)
		res.f = pars_color(str, res.f, res);
	if (str[0] == 'C' && str[1] == ' ' && (res.check.c = res.check.c + 1) < 2)
		res.c = pars_color(str, res.c, res);
	return (res);
}

int			sizemap(char **str)
{
	int i;

	i = 0;
	while (str[i] != 0)
		i++;
	return (i);
}
