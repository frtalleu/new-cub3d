/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser6.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 16:08:18 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/17 16:08:54 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

t_parse		set_player(t_parse res, char c, int i, int j)
{
	if (res.check.pos != 0)
	{
		res.check.pos++;
		return (res);
	}
	res.player.tp = 0;
	res.player.pos.x = (double)j + 0.5;
	res.player.pos.y = (double)i + 0.5;
	if (c == 'E')
		res = dir_for_east(res);
	else if (c == 'W')
		res = dir_for_weast(res);
	else if (c == 'S')
		res = dir_for_south(res);
	else if (c == 'N')
		res = dir_for_north(res);
	res.check.pos++;
	return (res);
}

t_parse		init_player(t_parse res)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (res.map[i] != 0)
	{
		while (res.map[i][j] != '\0')
		{
			if (res.map[i][j] == 'E' || res.map[i][j] == 'W'
				|| res.map[i][j] == 'S' || res.map[i][j] == 'N')
			{
				res = set_player(res, res.map[i][j], i, j);
				res.player.init_pos.x = j;
				res.player.init_pos.y = i;
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (res);
}

t_sprite	*get_sprite(t_parse res)
{
	int			i;
	int			j;
	t_sprite	*sprite_tab;

	if (!(sprite_tab = malloc(sizeof(t_sprite) * res.nb_sprite)))
		return (NULL);
	res.x = 0;
	i = -1;
	j = -1;
	while (res.map[++i] != 0)
	{
		while (res.map[i][++j] != '\0')
		{
			if (res.map[i][j] == '2' || res.map[i][j] == 'T')
			{
				sprite_tab[res.x].x = j + 0.5;
				sprite_tab[res.x].y = i + 0.5;
				sprite_tab[res.x].type = res.map[i][j];
				res.x++;
			}
		}
		j = -1;
	}
	return (sprite_tab);
}

t_parse		count_sprite(t_parse res)
{
	int j;
	int i;

	i = 0;
	j = 0;
	res.nb_sprite = 0;
	while (res.map[i] != 0)
	{
		while (res.map[i][j] != '\0')
		{
			if (res.map[i][j] == '2' || res.map[i][j] == 'T')
				res.nb_sprite++;
			j++;
		}
		j = 0;
		i++;
	}
	return (res);
}

t_parse		pars_map(char *line, t_parse res, int fd)
{
	char	*tmp;
	char	*tp;
	int		i;

	i = 1;
	tmp = ft_strjoin("~", line);
	free(line);
	while (i != 0)
	{
		i = get_next_line(fd, &line);
		check_line(line, res, i);
		tp = ft_strjoin(tmp, "~");
		free(tmp);
		tmp = ft_strjoin(tp, line);
		free(line);
		free(tp);
	}
	res.map = ft_split2(tmp, "~");
	free(tmp);
	res = init_player(res);
	res = count_sprite(res);
	res.sprite = get_sprite(res);
	return (res);
}
