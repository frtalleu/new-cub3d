/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser7.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 16:18:06 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/17 16:18:09 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

t_parse	parser(char *str)
{
	char	*line;
	int		fd;
	int		i;
	t_parse	res;

	fd = open(str, O_RDONLY);
	res = init_res();
	i = 1;
	while (i > 0)
	{
		i = get_next_line(fd, &line);
		if (line[0] != '\0' && line[0] != '1' && line[0] != ' '
				&& line[0] != '0')
			res = parstr(res, line);
		else if (line[0] == '1' || line[0] == ' ' || line[0] == '0')
		{
			res = pars_map(line, res, fd);
			break ;
		}
		free(line);
	}
	close(fd);
	res = erreur(res);
	return (res);
}
