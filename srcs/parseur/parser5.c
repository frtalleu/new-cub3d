/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser5.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 16:01:59 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/17 16:02:03 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

t_parse	dir_for_south(t_parse res)
{
	res.player.dir.x = 0;
	res.player.dir.y = 1;
	res.player.plane.x = -0.66;
	res.player.plane.y = 0;
	return (res);
}

t_parse	dir_for_north(t_parse res)
{
	res.player.dir.x = 0;
	res.player.dir.y = -1;
	res.player.plane.x = 0.66;
	res.player.plane.y = 0;
	return (res);
}

t_parse	dir_for_weast(t_parse res)
{
	res.player.dir.x = -1;
	res.player.dir.y = 0;
	res.player.plane.x = 0;
	res.player.plane.y = -0.66;
	return (res);
}

t_parse	dir_for_east(t_parse res)
{
	res.player.dir.x = 1;
	res.player.dir.y = 0;
	res.player.plane.x = 0;
	res.player.plane.y = 0.66;
	return (res);
}
