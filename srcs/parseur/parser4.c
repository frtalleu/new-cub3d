/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser4.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 15:54:35 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/17 15:54:39 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

void	check_line_config(char *str, t_parse res)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[i] != '\0' && str[i])
	{
		if (str[i] != ' ')
			j = 1;
		i++;
	}
	if ((!(str[0] == 'R' && str[1] == ' ') && !(str[0] == 'N' && str[1] == 'O'
	&& str[2] == ' ') && !(str[0] == 'S' && str[1] == 'O' && str[2] == ' ')
	&& !(str[0] == 'W' && str[1] == 'E' && str[2] == ' ') && !(str[0] == 'E'
	&& str[1] == 'A' && str[2] == ' ') && !(str[0] == 'S' && str[1] == ' ')
	&& !(str[0] == 'F' && str[1] == ' ') && !(str[0] == 'C'
	&& str[1] == ' ')) || (ft_strlen(str) != 0 && j != 1))
		ft_exit_bis(&res, "Error ligne config");
}

void	check_line(char *str, t_parse res, int j)
{
	int i;

	i = 0;
	if (ft_strlen(str) == 0 && j != 0)
		ft_exit_bis(&res, "Error ligne vide");
	while (str[i] != '\0')
	{
		if (str[i] != '0' && str[i] != ' ' && str[i] != '1' && str[i] != 'W' &&
				str[i] != 'E' && str[i] != 'N' && str[i] != 'S' && str[i] != '2'
				&& str[i] != 'T')
		{
			ft_exit_bis(&res, "Error ligne map invalide");
		}
		i++;
	}
}
