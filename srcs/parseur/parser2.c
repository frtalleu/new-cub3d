/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 12:41:00 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/17 12:41:02 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

t_player	player_init(void)
{
	t_player player;

	player.dir = vect_init();
	player.pos = vect_init();
	return (player);
}

t_check		check_init(void)
{
	t_check check;

	check.r = 0;
	check.no = 0;
	check.so = 0;
	check.we = 0;
	check.ea = 0;
	check.s = 0;
	check.f = 0;
	check.c = 0;
	check.map = 0;
	check.pos = 0;
	return (check);
}

t_color		color_init(void)
{
	t_color color;

	color.r = 0;
	color.g = 0;
	color.b = 0;
	return (color);
}

t_point		point_init(void)
{
	t_point pt;

	pt.x = 0;
	pt.y = 0;
	return (pt);
}

t_img		img_init(void)
{
	t_img img;

	img.img_ptr = NULL;
	img.data = NULL;
	return (img);
}
