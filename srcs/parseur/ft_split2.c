/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 06:15:17 by frtalleu          #+#    #+#             */
/*   Updated: 2020/04/20 19:08:12 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include <stdio.h>
#include <stdlib.h>
#include "../../cub3d.h"

static int	nb_word(char *str, char c)
{
	int		i;
	int		nb;

	i = 0;
	nb = 0;
	while (str[i] != '\0')
	{
		if (str[i] != c)
			nb++;
		while (str[i] != c && str[i] != '\0')
			i++;
		while (str[i] == c)
			i++;
	}
	return (nb);
}

char		**ft_split2(char *str, char *c)
{
	int		i;
	int		j;
	int		word;
	char	**tab;

	word = nb_word(str, c[0]);
	i = 0;
	if (!(tab = malloc(sizeof(char *) * (word + 1))))
		return (NULL);
	while (!(word = 0) && str[i] == c[0])
		i++;
	while (str[i] != '\0')
	{
		j = 0;
		while (str[i + j] != c[0] && str[i + j] != '\0')
			j++;
		while (str[i + j] == c[0])
			j++;
		if (str[i + j - 1] == c[0])
			str[i + j - 1] = '\0';
		tab[word++] = ft_strtrim(&str[i], c);
		i = i + j;
	}
	tab[word] = 0;
	return (tab);
}
