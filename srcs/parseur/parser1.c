/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/13 15:00:03 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/13 15:07:41 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "../libft/libft.h"
#include "../../cub3d.h"
#include "../GNL/get_next_line.h"

char		*pars_path(char *str, char *path, t_parse res)
{
	int i;
	int fd;

	i = 0;
	while (ft_isalpha(str[i]) != 0)
		i++;
	while (str[i] == ' ')
		i++;
	if (!(path = ft_strdup(&str[i])))
		return (NULL);
	if ((fd = open(path, O_RDONLY)) < 0)
		ft_exit_bis(&res, "Error Path invalide");
	close(fd);
	return (path);
}

t_parse		parse_r(char *str, t_parse res)
{
	int i;

	i = 1;
	res.r.x = ft_atoi(&str[i]);
	while (str[i] == ' ')
		i++;
	while (ft_isdigit(str[i]) == 1)
		i++;
	i++;
	while (str[i] != '\0' && ft_isdigit(str[i + 1]) == 0)
		i++;
	if (str[i] == '\0')
		ft_exit_bis(&res, "Error format taille ecran");
	res.r.y = ft_atoi(&str[i]);
	if (res.r.x == 0 || res.r.y == 0)
		ft_exit_bis(&res, "Error format taille ecran");
	return (res);
}

t_vect		vect_init(void)
{
	t_vect vecteur;

	vecteur.x = 0.0;
	vecteur.y = 0.0;
	return (vecteur);
}
