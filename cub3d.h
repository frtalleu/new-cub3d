/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/04 05:52:40 by frtalleu          #+#    #+#             */
/*   Updated: 2020/08/19 14:40:17 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H
# include <stddef.h>

# define X_SCREEN 2560
# define Y_SCREEN 1440
# define KEY_UP 119
# define KEY_UP_BIS 65362
# define KEY_DOWN 115
# define KEY_DOWN_BIS 65364
# define KEY_LEFT 97
# define KEY_RIGHT 100
# define KEY_TURN_L 65361
# define KEY_TURN_R 65363
# define KEY_MAP 109
# define KEY_ESC 65307

typedef struct	s_check
{
	int				r;
	int				no;
	int				so;
	int				we;
	int				ea;
	int				s;
	int				f;
	int				c;
	int				map;
	int				pos;
}				t_check;

typedef struct	s_point
{
	int				x;
	int				y;
}				t_point;

typedef struct	s_color
{
	int				r;
	int				g;
	int				b;
	int				c;
}				t_color;

typedef struct	s_img
{
	void		*img_ptr;
	int			*data;
	int			bpp;
	int			size_l;
	int			endian;
	int			width;
	int			height;
}				t_img;

typedef struct	s_key
{
	int			up;
	int			left;
	int			down;
	int			right;
	int			tl;
	int			tr;
	int			map;
}				t_key;

typedef struct	s_vect
{
	double		x;
	double		y;
}				t_vect;

typedef struct	s_player
{
	t_point		init_pos;
	t_vect		dir;
	t_vect		pos;
	t_vect		plane;
	int			tp;
}				t_player;

typedef struct	s_sprite
{
	double		x;
	double		y;
	double		dist;
	char		type;
	int			view;
	int			move;
	t_vect		dir;
}				t_sprite;

typedef struct	s_fsprite
{
	t_vect		sp;
	t_vect		transf;
	double		corr;
	t_point		sizex;
	t_point		sizey;
}				t_fsprite;

typedef struct	s_parse
{
	int			nb_sprite;
	t_sprite	*sprite;
	t_point		r;
	char		*no;
	char		*we;
	char		*ea;
	char		*s;
	char		*so;
	t_color		f;
	t_color		c;
	char		**map;
	t_check		check;
	void		*mlx_ptr;
	void		*win_ptr;
	t_img		img;
	t_key		keys;
	t_player	player;
	double		cam;
	t_vect		ray;
	t_vect		delta;
	int			wall;
	int			nsew;
	t_img		wno;
	t_img		wso;
	t_img		wwe;
	t_img		wea;
	t_img		text_sp;
	t_img		port;
	t_point		step;
	t_point		wall_pos;
	t_vect		side;
	double		wallx;
	int			wall_pixel;
	int			x;
	double		*buffer;
	t_fsprite	fsprite;
}				t_parse;

void			map_maybe_good(t_parse res);
void			screenshot(t_parse res);
void			ft_exit(t_parse *res, char *msg);
char			**ft_split2(char *str, char *c);
char			*pars_path(char *str, char *path, t_parse res);
t_parse			parse_r(char *str, t_parse res);
t_check			checkinit(void);
t_color			pars_color(char *str, t_color color, t_parse res);
t_parse			parstr(t_parse res, char *str);
int				sizemap(char **str);
t_parse			check_map(t_parse res);
t_parse			pars_map(char *line, t_parse res, int fd);
t_parse			parser(char *str);
t_parse			*get_pos(t_parse *res);
void			raycaster(t_parse *res);
char			*pars_path(char *str, char *path, t_parse res);
t_parse			parse_r(char *str, t_parse res);
t_vect			vect_init(void);
t_player		player_init(void);
t_check			check_init(void);
t_color			color_init(void);
t_point			point_init(void);
t_img			img_init(void);
t_key			init_key(void);
t_parse			init_res(void);
t_color			pars_color(char *str, t_color color, t_parse res);
int				sizemap(char **str);
t_parse			check_map(t_parse res);
int				count_space(char *str);
t_parse			dir_for_south(t_parse res);
t_parse			dir_for_north(t_parse res);
t_parse			dir_for_weast(t_parse res);
t_parse			dir_for_east(t_parse res);
t_parse			set_player(t_parse res, char c, int i, int j);
t_parse			init_player(t_parse res);
t_sprite		*get_sprite(t_parse res);
t_parse			count_sprite(t_parse res);
t_parse			pars_map(char *line, t_parse res, int fd);
t_parse			*get_dist(t_parse *res);
t_parse			*get_dist_wall(t_parse *res);
t_parse			*put_text(t_img tex, t_parse *res, int start, int end);
t_parse			*draw_column(t_parse *res);
double			draw_wall(t_parse *res);
t_parse			*sprite_dist(t_parse *res);
t_parse			*sort_sprite(t_parse *res);
t_parse			*sprite_to_img(t_parse *res, t_point size,
				int screen, t_img sprite);
t_parse			*get_size_y(t_parse *res, t_point size, int screen);
t_parse			*get_size_x(t_parse *res);
void			aff_sprite(t_parse *res);
void			make_sprite(t_parse *res);
t_parse			*get_where_lr(t_parse *res, double speed);
t_parse			*get_where(t_parse *res);
t_parse			*get_dir_right(t_parse *res, double speed);
t_parse			*get_dir_left(t_parse *res, double speed);
t_parse			*get_dir(t_parse *res);
t_parse			*make_tp(t_parse *res);
t_parse			*get_pos(t_parse *res);
int				x_map(char **str);
int				rgb_int(t_color color);
int				y_map(char **str);
t_parse			*make_rectangle(t_parse *res, int color, t_point cub,
				t_point pixel);
int				keyrelease(int key, t_parse *res);
int				keypress(int key, t_parse *res);
t_parse			*set_map(t_parse *res, t_point cub, int a);
t_parse			*draw_map(t_parse *res);
int				go_to_exit(t_parse *res);
void			make_img(t_parse *res);
int				draw(t_parse *res);
t_parse			init_texture(t_parse res);
void			draw_machine(t_parse res);
void			check_text(t_parse res);
void			ft_exit_bis(t_parse *res, char *msg);
t_parse			erreur(t_parse res);
void			check_line(char *str, t_parse res, int i);
void			check_line_config(char *str, t_parse res);
#endif
